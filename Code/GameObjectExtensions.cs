using UnityEngine;

namespace Hypnagogia.Extensions 
{
    public static class GameObjectExtensions
    {
        

        public static RectTransform GetRectTransform(this GameObject c) {
            return c.transform as RectTransform;
        }

        public static T GetOrAddComponent<T>(this GameObject child) where T : Component {
            T result = child.GetComponent<T>();
            if (result == null) {
                result = child.gameObject.AddComponent<T>();
            }
            return result;
        }

        public static T GetComponentInParentEvenHidden<T>(this GameObject gameObject) where T : UnityEngine.Object {
            Transform parent = gameObject.transform;
            T componet;
            while (true) {
                if (parent == null)
                    break;
                componet = parent.GetComponent<T>();
                if (componet != null)
                    return componet;
                else
                    parent = parent.parent;
            }
            return null;
        }
    }
}